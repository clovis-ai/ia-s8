from time import perf_counter

import chess

iteration_number = 0


def minimax(b, depth: int, max_score: int = 200, allied_player: bool = True) -> int:
	"""Déroulement d'une partie d'échecs au hasard des coups possibles. Cela va donner presque
	exclusivement des parties très longues et sans gagnant. Cela illustre cependant comment on peut
	jouer avec la librairie très simplement."""
	global iteration_number
	iteration_number += 1

	if b.is_game_over():
		results = {
			"1-0": max_score,
			"0-1": -max_score,
			"1/2-1/2": 0,
			"*": 0
		}
		return results[b.result()]

	if depth <= 0:
		return calculate_heuristic(b, max_score)

	score = -max_score if allied_player else max_score
	for move in b.legal_moves:
		b.push(move)
		score = (max if allied_player else min)(score, minimax(b, depth - 1))
		display(depth, score)
		b.pop()
	return score


def calculate_heuristic(b, max_score):
	scores = {
		chess.PAWN: 1,
		chess.KNIGHT: 3,
		chess.BISHOP: 3,
		chess.ROOK: 5,
		chess.QUEEN: 9,
		chess.KING: max_score // 2
	}
	score = 0
	for piece in b.piece_map().values():
		if piece.color == chess.WHITE:
			score += scores[piece.piece_type]
		else:
			score -= scores[piece.piece_type]
	return score


def display(depth, score):
	print("\033[" + str(depth) + ";0H" + str(score) + "    " + "\033[0;10H" + str(
		iteration_number))


board = chess.Board()
max_depth = 4

start = perf_counter()
minimax_score = minimax(board, max_depth)
end = perf_counter()

print("Depth", max_depth)
print("Time spent", end - start)
print("Score", minimax_score)
