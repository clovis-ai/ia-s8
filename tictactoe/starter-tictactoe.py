# -*- coding: utf-8 -*-

from random import choice
from time import perf_counter

import Tictactoe


def random_move(b):
	"""Renvoie un mouvement au hasard sur la liste des mouvements possibles"""
	return choice(b.legal_moves())


def deroulement_random(b):
	"""Effectue un déroulement aléatoire du jeu de morpion."""
	print("----------")
	print(b)
	if b.is_game_over():
		res = getresult(b)
		if res == 1:
			print("Victoire de X")
		elif res == -1:
			print("Victoire de O")
		else:
			print("Egalité")
		return
	b.push(random_move(b))
	deroulement_random(b)
	b.pop()


nombre_de_noeuds = 0
nombre_de_feuilles = 0
nombre_de_feuilles_gagnantes = 0
nombre_de_feuilles_perdantes = 0
nombre_de_feuilles_nulles = 0


def deroulement_total(b: Tictactoe.Board):
	"""Effectue un déroulement total du jeu de morpion."""
	global nombre_de_noeuds, nombre_de_feuilles, nombre_de_feuilles_perdantes, \
		nombre_de_feuilles_gagnantes, nombre_de_feuilles_nulles
	nombre_de_noeuds += 1
	print("----------")
	print(b)
	if b.is_game_over():
		nombre_de_feuilles += 1
		res = getresult(b)
		if res == 1:
			print("Victoire de X")
			nombre_de_feuilles_gagnantes += 1
		elif res == -1:
			print("Victoire de O")
			nombre_de_feuilles_perdantes += 1
		else:
			print("Egalité")
			nombre_de_feuilles_nulles += 1
		return
	for move in b.legal_moves():
		b.push(move)
		deroulement_total(b)
		b.pop()


def minimax(b: Tictactoe.Board, joueur_allie: bool = True) -> int:
	if b.is_game_over():
		return getresult(b)
	else:
		score = -10 if joueur_allie else 10
		for move in b.legal_moves():
			b.push(move)
			score = (max if joueur_allie else min)(score, minimax(b, not joueur_allie))
			b.pop()
		return score


def getresult(b: Tictactoe.Board):
	"""Fonction qui évalue la victoire (ou non) en tant que X.
	:returns 1 pour victoire, 0 pour égalité, -1 pour défaite"""
	if b.result() == b.player_x:
		return 1
	elif b.result() == b.player_o:
		return -1
	else:
		return 0


if __name__ == "__main__":
	board = Tictactoe.Board()

	start = perf_counter()
	best = minimax(board)
	end = perf_counter()

	print(
		"Apres le match, chaque coup est défait (grâce aux pop()): on retrouve le plateau de départ :")
	print(board)

	print("Temps total", end - start, "s")

	print("Score minimax", best)
